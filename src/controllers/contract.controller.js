const { Op } = require("sequelize");

const getContractById = async (req, res) => {
  const { profile } = req;
  const { Contract } = req.app.get('models');
  const { id } = req.params;

  try {
    const contract = await Contract.findOne({ where: {
      id,
      [Op.or]: [
        { ContractorId: profile.id },
        { ClientId: profile.id }
      ]
    }});
  
    if(!contract) return res.status(404).json({ error: 'Contract not found' });
  
    res.status(200).json({ contract });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

getAllContractByProfile = async (req, res) => {
  const { profile } = req;
  const { Contract } = req.app.get('models');

  try {
    const contracts = await Contract.findAll({ where: {
      [Op.not]: {
        status: 'terminated'
      },
      [Op.or]: [
        { ContractorId: profile.id },
        { ClientId: profile.id }
      ]
    }});
  
    if(!contracts) return res.status(404).end();
  
    res.status(200).json({ contracts });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  getContractById,
  getAllContractByProfile
};