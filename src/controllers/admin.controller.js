const Sequelize = require("sequelize");

const getBestProfession = async (req, res) => {
  const { Profile } = req.app.get('models');
  const { start, end } = req.query;

  try {
    const result = await Profile.findOne({
      attributes: [
        'id',
        'firstName',
        'lastName',
        'profession',
        [Sequelize.literal(`(
          SELECT SUM("price")
          FROM "Jobs" INNER JOIN "Contracts" ON "Jobs"."ContractId" = "Contracts"."id"
          WHERE "Contracts"."ContractorId" = "Profile"."id"
            AND "Jobs"."paid" = true
            AND "Jobs"."paymentDate" BETWEEN :start AND :end)`
          ), 'totalEarnings'],
      ],
      replacements: { start, end },
      order: [[Sequelize.literal('totalEarnings'), 'DESC']],
    });

    res.status(200).json(result);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

const getBestClients = async (req, res) => {
  const { Profile } = req.app.get('models');
  const { start, end, limit } = req.query;

  try {
    const result = await Profile.findAll({
      attributes: [
        'id',
        'firstName',
        'lastName',
        [Sequelize.literal(`(
          SELECT SUM("price")
          FROM "Jobs" INNER JOIN "Contracts" ON "Jobs"."ContractId" = "Contracts"."id"
          WHERE "Contracts"."ClientId" = "Profile"."id"
            AND "Jobs"."paid" = true
            AND "Jobs"."paymentDate" BETWEEN :start AND :end)`
        ), 'totalPayments'],
      ],
      replacements: { start, end },
      order: [[Sequelize.literal('totalPayments'), 'DESC']],
      limit: parseInt(limit),
    });

    res.status(200).json(result);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports = {
  getBestProfession,
  getBestClients
};
