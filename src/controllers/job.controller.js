const { Op } = require("sequelize");

const getUnpaidJobsByProfile = async (req, res) => {
  const { profile } = req;
  const { Job, Contract } = req.app.get('models');

  const result = await Job.findAll({
    include: {
      model: Contract,
      where: {
        status: 'in_progress',
        [Op.or]: [
          { ContractorId: profile.id },
          { ClientId: profile.id }
        ]
      }
    },
    where: {
      paid: {
        [Op.not]: true
      },
    }
  });

  res.status(200).json(result);
};

const payAJob = async (req, res) => {
  const sequelize = req.app.get('sequelize');
  const { profile } = req;
  const { Job, Contract, Profile } = req.app.get('models');
  const { job_id } = req.params;

  // Starting the transaction
  try {
    await sequelize.transaction(async (t) => {
      // Getting the job and the related parts of the contract
      const job = await Job.findOne({
        where: { id: job_id },
        include: {
          model: Contract,
          where: {
            status: 'in_progress',
            ClientId: profile.id
          },
          include: [{
            model: Profile,
            as: 'Client'
          }, {
            model: Profile,
            as: 'Contractor'
          }]
        },
        transaction: t
      });
    
      // User feedback
      if (!job) return res.status(404).json({ error: 'Job not found' });
      if (job.paid) return res.status(406).json({ error: 'Job is already paid' });
    
      const client = job.Contract.Client;
      const contractor = job.Contract.Contractor;
      const contract = job.Contract;
    
      if (job.price > client.balance) return res.status(406).json({ error: 'Insufficient money' });

      // Updating the balances
      await client.update({ balance: (client.balance - job.price) }, { transaction: t });
      await contractor.update({ balance: (contractor.balance + job.price) }, { transaction: t });

      // Updating the job
      await job.update({ paid: true }, { transaction: t });

      // Updating the contract; I don't know if this is correct in terms of business rules.
      await contract.update({ status: 'terminated' }, { transaction: t })
    });

    res.status(200).json({ message: 'Operation completed' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  getUnpaidJobsByProfile,
  payAJob
};