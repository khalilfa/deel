const { Op } = require('sequelize');

const depositMoney = async (req, res) => {
  const sequelize = req.app.get('sequelize');
  const { Profile, Job, Contract } = req.app.get('models');
  const { userId } = req.params;
  const { amount } = req.body;

  // Starting the transaction
  try {
    await sequelize.transaction(async (t) => {
      const jobs = await Job.findAll({
        where: {
          paid: {
            [Op.not]: true
          }
        },
        include: {
          model: Contract,
          where: {
            [Op.not]: {
              status: 'terminated'
            },
          },
          include: {
            model: Profile,
            as: 'Client',
            where: {
              id: userId
            }
          }
        },
        transaction: t
      });
  
      if (jobs.length == 0) return res.status(404).json({ error: 'The client has no jobs to pay for' });

      // Calculate the 25%
      const totalJobsAmount = jobs.reduce((sum, job) => sum + job.price, 0);
      const maxDepositAmount = totalJobsAmount * 0.25;

      // Verify the amount
      if (amount > maxDepositAmount) return res.status(406).json({ error: 'Deposit amount exceeds 25% available' });

      // Get the client
      const jobWithClient = jobs.find(job => job.Contract && job.Contract.Client);

      if (!jobWithClient) return res.status(404).json({ error: 'Client not found' });

      const client = jobWithClient.Contract.Client;

      // Update the balance
      const newBalance = client.balance + amount;
      await client.update({ balance: newBalance }, { transaction: t });

      res.status(200).json({ message: 'Successful deposit', balance: newBalance });
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  depositMoney
}
