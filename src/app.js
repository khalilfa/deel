const express = require('express');
const bodyParser = require('body-parser');
const sequelize = require('./db/sequelize');

const contractsRouter = require('./routes/contracts.router');
const jobRouter = require('./routes/job.router');
const adminRouter = require('./routes/admin.router');
const balanceRouter = require('./routes/balance.router');

const app = express();

app.use(bodyParser.json());
app.set('sequelize', sequelize)
app.set('models', sequelize.models)

app.use('/contracts', contractsRouter);
app.use('/jobs', jobRouter);
app.use('/admin', adminRouter);
app.use('/balance', balanceRouter);

module.exports = app;
