const balanceController = require('../controllers/balance.controller');

const router = require('express').Router();

router.post('/deposit/:userId', balanceController.depositMoney);

module.exports = router;