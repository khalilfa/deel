const { getProfile } = require('../middlewares/getProfile');
const jobController = require('../controllers/job.controller');

const router = require('express').Router();

router.get('/unpaid', getProfile, jobController.getUnpaidJobsByProfile);
router.post('/:job_id/pay', getProfile, jobController.payAJob);

module.exports = router;