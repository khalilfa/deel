const adminController = require('../controllers/admin.controller');

const router = require('express').Router();

router.get('/best-profession', adminController.getBestProfession);
router.get('/best-clients', adminController.getBestClients);

module.exports = router;