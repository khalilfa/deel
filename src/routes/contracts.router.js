const { getProfile } = require('../middlewares/getProfile');
const contractController = require('../controllers/contract.controller');

const router = require('express').Router();

router.get('/', getProfile, contractController.getAllContractByProfile);
router.get('/:id', getProfile, contractController.getContractById);

module.exports = router;