const Sequelize = require('sequelize');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite3'
});

const models = [
  require('../models/job.model'),
  require('../models/profile.model'),
  require('../models/contract.model'),
];

models.map(model => model(sequelize)).map(model => model.associate(sequelize.models));

module.exports = sequelize;